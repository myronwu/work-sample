# Tsuru Capital Work Sample

This shows pretty well a snapshot of my current level of understanding in Haskell.

## Assumptions and Design Notes

- The quote accept times are in KST (UTC+9) since the feed is from KOSPI
- There is always enough memory to hold a 3 second buffer of packet data. If not, we'd need to fix the size of the buffer. This would trade correctness for availability.
- We do a stable sort of the packet stream; packet order is preserved for quotes with the same accept time.
- Conduit was used over other streaming libraries arbitrarily, not for any difference in performance measured.

## Performance

Throughput is measured at around 12,000 packets/sec on a [2.7 GHz Intel Core i7 with 16GB RAM MacBook Pro](https://support.apple.com/kb/SP653?locale=en_US).

## Build

This builds with [cabal](https://www.haskell.org/cabal/users-guide/), [nix](https://nixos.org/nix/manual/), and [stack](https://docs.haskellstack.org/en/stable/README/).
