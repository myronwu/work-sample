{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import           Conduit
import           Control.Applicative (pure)
import           Data.List as List
import           Data.Time.Calendar (fromGregorianValid)
import           Data.Time.Clock (UTCTime(..), NominalDiffTime, addUTCTime)
import           Data.Time.LocalTime (TimeOfDay, timeToTimeOfDay, utcToLocalTimeOfDay)
import           Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import           Sample.Main (sortInLagWindow)
import           Sample.Kospi (BidAsk(..), Message(..), Packet(..))
import           Sample.Kospi.Parser (quoteAcceptTimeToUTC, quoteAcceptTimeZone)
import           Test.Tasty (defaultMain, testGroup)
import           Test.Tasty.Hedgehog (testProperty)


main :: IO ()
main =
 defaultMain $
 testGroup "tasty-hedgehog tests"
   [ testProperty "sortInLagWindow sorts the stream"
                  prop_sortInLagWindow_sorts
   , testProperty "quoteAcceptTimeToUTC converts the accept time to UTC using the packet time for date information"
                  prop_quoteAcceptTimeToUTC_converts_to_utc
   ]

prop_sortInLagWindow_sorts :: Property
prop_sortInLagWindow_sorts =
  property $ do
    -- this is a bit wasteful of memory, but makes for nicer output on failure than a (mapC (<=) .| andC) sort check
    let conduitToSortedList c = c .| sortInLagWindow .| sinkList
    list <- forAll $ runConduit (conduitToSortedList (genPacketConduit (Range.linear 0 500)))
    list === List.sort list

prop_quoteAcceptTimeToUTC_converts_to_utc :: Property
prop_quoteAcceptTimeToUTC_converts_to_utc =
  property $ do
    packetTimeLag <- forAll genPacketTimeLag
    packetTime    <- forAll genUTCTime
    let acceptTime = addUTCTime packetTimeLag packetTime
    quoteAcceptTimeToUTC packetTime (utcTimeOfDay acceptTime) === acceptTime

utcTimeOfDay :: UTCTime -> TimeOfDay
utcTimeOfDay = snd . utcToLocalTimeOfDay quoteAcceptTimeZone . timeToTimeOfDay . utctDayTime

genPacketTimeLag :: MonadGen m => m NominalDiffTime
genPacketTimeLag = fromIntegral <$> Gen.int (Range.linear (-3) 0)

genPacketConduit :: MonadGen m => Range.Range Int -> ConduitM () Packet m ()
genPacketConduit r = do
  firstPacket     <- lift genPacket
  packetTimeDiffs <- lift $ Gen.list r (realToFrac @Float @NominalDiffTime <$> Gen.realFloat (Range.linearFrac 0.0 5.0))
  loop firstPacket packetTimeDiffs
  where
     loop p []     = yield p
     loop p (t:ts) = do
       yield p
       p' <- lift $ genPacketAtTime (addUTCTime t (packetTime p))
       loop p' ts

genPacket :: MonadGen m => m Packet
genPacket = genUTCTime >>= \t -> Packet t <$> genMessage t

genPacketAtTime :: MonadGen m => UTCTime -> m Packet
genPacketAtTime t = Packet t <$> genMessage t

genMessage :: MonadGen m => UTCTime -> m Message
genMessage packetTime = do
  packetTimeLag                  <- genPacketTimeLag
  issueCode                      <- Gen.string (Range.singleton 12) Gen.alphaNum
  [bid1, bid2, bid3, bid4, bid5] <- Gen.list (Range.singleton 5) genBidAsk
  [ask1, ask2, ask3, ask4, ask5] <- Gen.list (Range.singleton 5) genBidAsk
  pure Message
    { acceptTime = addUTCTime packetTimeLag packetTime
    , issueCode
    , bid1, bid2, bid3, bid4, bid5
    , ask1, ask2, ask3, ask4, ask5 }

genBidAsk :: MonadGen m => m (BidAsk a)
genBidAsk = do
  price <- show <$> Gen.int (Range.linear 0 99999)
  qty   <- show <$> Gen.int (Range.linear 0 9999999)
  pure BidAsk { price, qty }

genUTCTime :: MonadGen m => m UTCTime
genUTCTime = UTCTime <$> day <*> time
  where
    year       = Gen.integral $ Range.linear 0 9999
    month      = Gen.int $ Range.linear 1 12
    dayOfMonth = Gen.int $ Range.linear 1 31
    day        = Gen.just (fromGregorianValid <$> year <*> month <*> dayOfMonth)
    time       = fromIntegral <$> Gen.int (Range.linear 0 86401)
