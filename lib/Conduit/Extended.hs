{-# LANGUAGE BangPatterns #-}

module Conduit.Extended
  ( module Conduit
  , eitherC
  , printStderrC
  , concatMapAccumFinalC
  ) where

import Conduit
import Data.Void
import System.IO (hPrint, stderr)

-- |This is an end of stream-aware version of 'Conduit.concatMapAccumC'.
-- See <https://mail.haskell.org/pipermail/haskell-cafe/2017-July/127589.html> for alternatives and the
-- explanation of how 'Conduit.concatMapAccumC' doesn't handle the end of stream, throwing away the last accum.
concatMapAccumFinalC
  :: Monad m
  => (a -> accum -> (accum, [b]))  -- ^ A function that merges an item and returns the next accumulator with a chunk of zero or more items.
  -> (accum -> [b])                -- ^ A function to call at the end of the stream to handle the last chunk of items.
  -> accum                         -- ^ The starting value for the accumulator.
  -> Conduit a m b
concatMapAccumFinalC step finish = loop
  where
    loop !s = do
      mItem <- await
      case mItem of
        Nothing -> yieldMany $ finish s
        Just x -> do
          let (s', ss) = step x s
          yieldMany ss
          loop s'

-- |Stderr analogue of 'Conduit.printC'.
printStderrC :: Show a => Consumer a IO ()
printStderrC = mapM_C $ hPrint stderr

-- |Split a stream to left and right. This is done internally with ZipSink.
eitherC
  :: Monad m
  => ConduitM a Void m ()    -- ^ Left conduit.
  -> ConduitM b Void m ()    -- ^ Right conduit.
  -> Sink (Either a b) m ()
eitherC f g = getZipSink (ZipSink (concatMapC left .| f) *> ZipSink (concatMapC right .| g))
  where
    left = either Just (const Nothing)
    right = either (const Nothing) Just
