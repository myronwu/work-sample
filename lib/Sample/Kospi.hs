{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE NamedFieldPuns #-}

module Sample.Kospi where

import Data.Semigroup ((<>))
import Data.List (intercalate)
import Data.Ord (Ord, compare)
import Data.Time.Clock (UTCTime)

-- |The timezone offset from UTC. KOSPI serves Korea, which uses KST: UTC+9, DST-less since 1988
timeZoneOffset :: Int
timeZoneOffset = 9

-- |The maximum time difference we expect between quote accept and packet times.
maxPacketLag :: Int
maxPacketLag = 3

data Packet
  = Packet
  { packetTime :: UTCTime
  , message :: Message
  } deriving Eq

data Message
  = Message
  { acceptTime :: UTCTime
  , issueCode :: String
  , bid1 :: Bid', bid2 :: Bid', bid3 :: Bid', bid4 :: Bid', bid5 :: Bid'
  , ask1 :: Ask', ask2 :: Ask', ask3 :: Ask', ask4 :: Ask', ask5 :: Ask'
  } deriving (Eq, Show)

instance Ord Packet where
  compare p1 p2 = compare (acceptTime $ message p1) (acceptTime $ message p2)

instance Show Packet where
  show Packet { packetTime, message } =
    let Message { acceptTime
                , issueCode
                , bid1, bid2, bid3, bid4, bid5
                , ask1, ask2, ask3, ask4, ask5 } = message
        tabSeparate = intercalate "\t"
    in
      tabSeparate [ show packetTime
                  , show acceptTime
                  , issueCode
                  , tabSeparate (fmap show [bid5, bid4, bid3, bid2, bid1])
                  , tabSeparate (fmap show [ask1, ask2, ask3, ask4, ask5]) ]

-- |Bids and asks don't differ by show or record fields; they only differ conceptually, so we represent them
-- with the same type differentiated by a phantom type.
data BidAsk (a :: BidAskType)
  = BidAsk
  { qty :: String
  , price :: String
  } deriving Eq

instance Show (BidAsk a) where
  show BidAsk { qty, price } = qty <> "@" <> price

data BidAskType = Bid | Ask

type Bid' = BidAsk 'Bid
type Ask' = BidAsk 'Ask
