{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Sample.Kospi.Parser where

import           Data.Attoparsec.ByteString.Char8 (Parser, (<?>), anyChar, char, count, digit, parseOnly, string)
import qualified Data.Attoparsec.ByteString.Char8 as Attoparsec
import           Data.ByteString (ByteString)
import           Data.Semigroup ((<>))
import           Data.Time.Calendar (addDays)
import           Data.Time.Clock (UTCTime(..))
import           Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import           Data.Time.Format (defaultTimeLocale, parseTimeM)
import           Data.Time.LocalTime ( TimeOfDay
                                     , TimeZone
                                     , hoursToTimeZone
                                     , localToUTCTimeOfDay
                                     , timeOfDayToTime
                                     , timeToTimeOfDay
                                     , todHour)
import           Network.Pcap (PktHdr(..), hdrDiffTime)
import qualified Network.Pcap.Conduit as Pcap
import           Sample.Kospi (BidAsk(..), Bid', Ask', Packet(..), Message(..))
import qualified Sample.Kospi as Kospi


parsePacket :: Pcap.Packet -> Either String Packet
parsePacket (pktHdr, bs) = Packet packetTime <$> parsePayload packetTime bs
  where packetTime = packetTimeInUTC pktHdr

parsePayload
  :: UTCTime
  -> ByteString
  -> Either String Message
parsePayload packetTime = parseOnly $ do
  _                              <- Attoparsec.take 42               <?> "packet header"
  _                              <- string "B6034"                   <?> "message type code"
  issueCode                      <- count 12 anyChar                 <?> "issue code"
  _                              <- Attoparsec.take 12               <?> "issue seq no, status type, bid quote volume"
  [bid1, bid2, bid3, bid4, bid5] <- count 5 bidParser                <?> "bids"
  _                              <- Attoparsec.take 7                <?> "ask quote volume"
  [ask1, ask2, ask3, ask4, ask5] <- count 5 askParser                <?> "asks"
  _                              <- Attoparsec.take 50               <?> "best bid and ask numbers"
  acceptTime                     <- quoteAcceptTimeParser packetTime <?> "quote acceptTime"
  _                              <- char '\255'                      <?> "0xff end of message"
  return Message
    { acceptTime
    , issueCode
    , bid1, bid2, bid3, bid4, bid5
    , ask1, ask2, ask3, ask4, ask5 }

quoteAcceptTimeParser :: UTCTime -> Parser UTCTime
quoteAcceptTimeParser packetTime = do
  hhmmss    <- count 6 digit                                                       <?> "accept time hhmmss"
  uu        <- count 2 digit                                                       <?> "accept time uu"
  timeOfDay <- parseTimeM False defaultTimeLocale "%H%M%S%Q" (hhmmss <> "." <> uu) <?> "accept time as time of day"
  return $ quoteAcceptTimeToUTC packetTime timeOfDay

quoteAcceptTimeZone :: TimeZone
quoteAcceptTimeZone = hoursToTimeZone Kospi.timeZoneOffset

quoteAcceptTimeToUTC
  :: UTCTime         -- ^ The packet time of the message. This will be used as the reference point for the day and year.
  -> TimeOfDay       -- ^ The quote accept time of day.
  -> UTCTime         -- ^ The quote accept time.
quoteAcceptTimeToUTC packetTime acceptTime =
  case (packetHour, acceptHour) of
    -- packet and quote accept time shouldn't differ by > 3 seconds, so the hour will never be more than 1 apart
    (23, 0) -> UTCTime { utctDay = addDays   1  $ utctDay packetTime, utctDayTime = acceptDayTime }
    (0, 23) -> UTCTime { utctDay = addDays (-1) $ utctDay packetTime, utctDayTime = acceptDayTime }
    _       -> UTCTime { utctDay =                utctDay packetTime, utctDayTime = acceptDayTime }
  where
    packetHour = todHour . timeToTimeOfDay $ utctDayTime packetTime
    acceptHour = todHour acceptTimeOfDay
    acceptDayTime = timeOfDayToTime acceptTimeOfDay
    (_, acceptTimeOfDay) = localToUTCTimeOfDay quoteAcceptTimeZone acceptTime

packetTimeInUTC :: PktHdr -> UTCTime
packetTimeInUTC = posixSecondsToUTCTime . realToFrac . hdrDiffTime  -- hdrTime is in microseconds

bidParser :: Parser Bid'
bidParser = bidAskParser

askParser :: Parser Ask'
askParser = bidAskParser

bidAskParser :: Parser (BidAsk ba)
bidAskParser = do
  price <- count 5 digit <?> "price"
  qty   <- count 7 digit <?> "quantity"
  return BidAsk { qty = stripLeadingZeroes qty, price = stripLeadingZeroes price }
  where
    stripLeadingZeroes cs =
      case span (== '0') cs of
        (_, [])  -> "0"
        (_, cs') -> cs'