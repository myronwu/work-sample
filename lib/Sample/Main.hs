{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeApplications #-}

module Sample.Main where

import           Conduit.Extended
import           Control.Monad (when)
import qualified Data.PQueue.Min as PQ
import           Data.Semigroup ((<>))
import           Data.Time.Clock (NominalDiffTime, UTCTime, diffUTCTime)
import           Data.Tuple (swap)
import           Network.Pcap.Conduit (sourceOffline)
import           Options.Applicative
import           System.IO (FilePath)
import           Sample.Kospi (Packet, acceptTime, message, packetTime)
import qualified Sample.Kospi as Kospi
import           Sample.Kospi.Parser (parsePacket)


main :: IO ()
main = processPackets =<< execParser opts
  where
    opts = info (optParser <**> helper)
           ( fullDesc
          <> progDesc "Print market messages from a pcap file FILE" )
    optParser =
      Config
        <$> switch
            ( short 'r'
           <> help "Whether to reorder messages by quote accept time" )
        <*> switch
            ( short 'v'
           <> help "Whether to show parse errors on stderr" )
        <*> strArgument
            ( metavar "FILE" )

data Config
  = Config
  { reorderMessages :: Bool
  , verbose :: Bool
  , file :: FilePath }

-- |Process packets from a pcap dump file, optionally re-ordering packets in a 3 second window.
-- Unrecognized packets can optionally be printed to stderr.
processPackets :: Config -> IO ()
processPackets Config { reorderMessages, verbose, file } =
  runConduit
   $ sourceOffline file
  .| mapC parsePacket
  .| eitherC (when verbose printStderrC)
             (if reorderMessages
                then sortInLagWindow .| printC
                else printC)

-- |Sort inside a window of 'Kospi.maxPacketLag' between packet time and quote accept time.
-- This will accumulate packets into a priority queue, releasing any packets with accept times
-- that are greater than 'Kospi.maxPacketLag' from the most recently seen packet.
sortInLagWindow
  :: Monad m
  => Conduit Packet m Packet
sortInLagWindow = concatMapAccumFinalC accumPacket PQ.toList PQ.empty
  where
    accumPacket :: Packet -> PQ.MinQueue Packet -> (PQ.MinQueue Packet, [Packet])
    accumPacket packet queue =
      let queue' = PQ.insertBehind packet queue in  -- insertBehind to break ties with packet order
        pqSpan (\p -> diffExceedsMaxLag (packetTime packet) (acceptTime . message $ p)) queue'

    pqSpan :: Ord a => (a -> Bool) -> PQ.MinQueue a -> (PQ.MinQueue a, [a])
    pqSpan predicate = swap . PQ.span predicate

    diffExceedsMaxLag :: UTCTime -> UTCTime -> Bool
    diffExceedsMaxLag t1 t2 = abs (diffUTCTime t1 t2) > fromIntegral @Int @NominalDiffTime Kospi.maxPacketLag
