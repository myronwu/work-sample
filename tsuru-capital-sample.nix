{ mkDerivation, attoparsec, base, bifunctors, bytestring
, conduit-combinators, hedgehog, optparse-applicative, pcap
, pcap-conduit, pqueue, stdenv, tasty, tasty-hedgehog, text, time
}:
mkDerivation {
  pname = "tsuru-capital-sample";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    attoparsec base bifunctors bytestring conduit-combinators pcap
    pcap-conduit pqueue text time
  ];
  executableHaskellDepends = [ base optparse-applicative ];
  testHaskellDepends = [
    base conduit-combinators hedgehog tasty tasty-hedgehog time
  ];
  description = "My attempt at solving Tsuru Capital's code sample problem from http://www.tsurucapital.com/en/code-sample.html";
  license = stdenv.lib.licenses.bsd3;
}
